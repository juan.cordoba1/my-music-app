import { fetchApi } from "../../helpers/fetch"
import { types } from "../types/types";

import Swal from 'sweetalert2';

export const getPlaylist = (token) => {

    return async (dispatch) => {

        try {
            const resp = await fetchApi( 'https://api.spotify.com/v1/me/playlists?limit=20&offset=0', 'GET', token );
            const data = await resp.json()

            const { items } = data
            dispatch( playlistSave( items ) )
        } catch (error) {
            console.log('Error...')
        }

    }

}

export const playlistSave = ( playlists ) => ({
    type: types.muscicGetPlalists,
    payload: playlists
})

// Obtener canciones de una playlist especifica
export const getTracksPlaylist = (token, playlistId) => {

    return async (dispatch) => {

        try {
            const resp = await fetchApi( `https://api.spotify.com/v1/playlists/${ playlistId }/tracks?limit=20&offset=0`, 'GET', token );
            const data = await resp.json()

            const { items } = data;
            let tracks = [];
            items.forEach( item => {
                tracks.push( item.track )
            })

            const updateTracks = await checkCurrentTrack(tracks)
            
            dispatch( tracksSave( updateTracks ) );    
        } catch (error) {
            console.log('Error...');
        }

    }

}

export const tracksSave = ( tracks ) => ({
    type: types.musicGetTracksPlaylist,
    payload: tracks
})

// Quitar el track del store
export const tracksDelete = () => ({
    type: types.musicDeleteTracksPlaylist
})

export const setCurrentPlaylist = ( playlistId ) => ({
    type: types.musicCurrentPlaylist,
    payload: playlistId
})

// Quitar el currentPlaylist del store
export const deleteCurrentPlaylist = () => ({
    type: types.musicDeleteCurrentPlaylist
})

// Obtener canciones favoritas del usuario
export const getFavoritesTracks = (token) => {

    return async (dispatch) => {

        try {
            const resp = await fetchApi( 'https://api.spotify.com/v1/me/tracks?limit=35&offset=0', 'GET', token )
            const data = await resp.json()

            const { items } = data;
            let tracks = items.map( item => item.track )

            const updateTracks = await checkCurrentTrack(tracks);

            dispatch(favoritesTracksSave( updateTracks ))
        } catch (error) {
            console.log('Error...');
        }

    }

}

export const favoritesTracksSave = (tracks) => ({
    type: types.musicSaveFavoritesTracks,
    payload: tracks
})

// Quitar el favoritestracks del store
export const favoritesTracksDeleteStore = () => ({
    type: types.musicDeleteFavoritesTracksStore
})

export const musicLogout = () => ({
    type: types.musicLogout
})

// Guardar cancion en lista de favoritas del usuario
export const setFavoritesTracks = (token, idTrack) => {

    return async (dispatch) => {

        try {
            const resp = await fetchApi( `https://api.spotify.com/v1/me/tracks?ids=${ idTrack }`, 'PUT', token )

            if( resp.ok ) {
                /* dispatch(getFavoritesTracks(token)) */
                Swal.fire({
                    customClass: {
                        popup: 'popup__class',
                        title: 'title__class',
                        content: 'content__class'
                    },
                    icon: 'success',
                    title: 'Added to your favorites.',
                    background: 'rgba(20, 20, 20, 0.97)',
                    timer: 2000,
                    timerProgressBar: true,
                    toast: true,
                    position: 'bottom',
                    showConfirmButton: false
                })
            }

        } catch (error) {
            console.log('Error...')
        }

    }

}

// Eliminar cancion de lista de favoritas del usuario
export const DeleteFavoriteTrack = (token, idTrack) => {

    return async (dispatch) => {

        try {
            const resp = await fetchApi( `https://api.spotify.com/v1/me/tracks?ids=${ idTrack }`, 'DELETE', token )

            if( resp.ok ) {
                /* dispatch(getFavoritesTracks(token)) */
                Swal.fire({
                    customClass: {
                        popup: 'popup__class',
                        title: 'title__class',
                        content: 'content__class'
                    },
                    icon: 'success',
                    title: 'Removed from your favorites.',
                    background: 'rgba(20, 20, 20, 0.97)',
                    timer: 2000,
                    timerProgressBar: true,
                    toast: true,
                    position: 'bottom',
                    showConfirmButton: false
                })
            }

        } catch (error) {
            console.log('Error...')
        }

    }

}

// Comprobar si una cancion esta en la lista de favoritas
export const checkCurrentTrack = async (tracks=[]) => {
    
    try {

        let listIds = tracks.map( track => track.id)

        const resp = await fetchApi( `https://api.spotify.com/v1/me/tracks/contains?ids=${ listIds }`, 'GET' )
        const data = await resp.json()

        return tracks.map( (track, index) => ({...track, isInFavorites: data[index]}) )

    } catch (error) {
        console.log('Error...')
    }

}

/* export const saveStoreCurrentTrack = (track) => ({
    type: types.musicSaveStoreCurrentTrack,
    payload: track
}) */