import { fetchApi } from "../../helpers/fetch"
import { types } from "../types/types";

// Obtener info de usuario de la api
export const getUserProfile = (token) => {

    return async ( dispatch ) => {

        try {
            const resp = await fetchApi('https://api.spotify.com/v1/me', 'GET', token);
            const data = await resp.json();

            const { id, display_name, email } = data
            let user = { id, display_name, email };
            dispatch( userProfileSave( user, token ) )

        } catch (error) {
            console.log('Error...')
        }
    }

}

export const userProfileSave = ( user, token ) => ({
    type: types.authGetUser,
    payload: {
        token,
        user
    }
});

export const authLogout = () => ({
    type: types.authLogout
});