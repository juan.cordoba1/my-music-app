
export const types = {

    authGetUser: '[Auth] Get user',
    authLogout: '[Auth] Logout',

    muscicGetPlalists: '[Music] Get playlist',
    musicGetTracksPlaylist: '[Music] Get TracksPlaylist',
    musicDeleteTracksPlaylist: '[Music] Delete TracksPlaylist',
    
    musicCurrentPlaylist: '[Music] Set currentPlaylist',
    musicDeleteCurrentPlaylist: '[Music] Delete currentPlaylist',

    musicSaveFavoritesTracks: '[Music] Save favoritesTracks',
    musicDeleteFavoritesTracksStore: '[Music] Delete favoritesTracksStore',
    musicSaveStoreCurrentTrack: '[Music] Save currentTrack',

    musicLogout: '[Music] Logout',

}