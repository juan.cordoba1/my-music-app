import { types } from "../types/types";

const initialState = {
    playlists: null,
    tracks: null,
    currentPlaylist: null,
    favoritesTracks: null
}

export const musicReducer = ( state = initialState, action ) => {

    switch (action.type) {
        case types.muscicGetPlalists:
            return {
                ...state,
                playlists: action.payload
            }
        case types.musicGetTracksPlaylist:
            return {
                ...state,
                tracks: action.payload
            }
        case types.musicCurrentPlaylist:
            return {
                ...state,
                currentPlaylist: action.payload
            }
        case types.musicDeleteCurrentPlaylist:
            return {
                ...state,
                currentPlaylist: null
            }
        case types.musicDeleteTracksPlaylist:
            return {
                ...state,
                tracks: null
            }
        case types.musicSaveFavoritesTracks:
            return {
                ...state,
                favoritesTracks: action.payload
            }
        case types.musicDeleteFavoritesTracksStore:
            return {
                ...state,
                favoritesTracks: null
            }
        /* case types.musicSaveStoreCurrentTrack:
            return {
                ...state,
                isInFavorites: !action.payload
            } */
        case types.musicLogout:
            return {}
        
        default:
            return state;
    }

}