import { types } from "../types/types";

export const authReducer = ( state = {}, action ) => {

    switch (action.type) {
        case types.authGetUser:
            return {
                user: action.payload
            }
        case types.authLogout:
            return { }   
    
        default:
            return state;
    }

}