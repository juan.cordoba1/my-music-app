import React, { useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import { Favorites } from '../pages/Favorites';
import { Home } from '../pages/Home';
import { PlaylistTracks } from '../pages/PlaylistTracks';
import { AuthRouter } from './AuthRouter';

export const AppRouter = () => {

    return (
        <Router>
            <div>
                <Switch>
                    
                    <Route exact path='/' component={ Home } />
                    <Route exact path='/favorites' component={ Favorites } />
                    <Route exact path='/playlist/:playlistId' component={ PlaylistTracks } />
                    <Route path='/auth' component={ AuthRouter } />                  
                  
                    <Redirect to="/auth/login" />
                </Switch>
            </div>
            
        </Router>
    )
}
