import React from 'react'
import { Header } from '../../molecules/header/Header'
import { NavBar } from '../../atoms/navBar/NavBar'

export const Playlist = ({ children }) => {
    return (
        <div className='playlist__main'>
            <Header />
            <NavBar />

            {children}
        </div>
    )
}
