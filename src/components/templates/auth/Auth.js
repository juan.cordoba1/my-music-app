import React from 'react'
import { LoginLogo } from '../../atoms/LoginLogo/LoginLogo'

export const Auth = ({children}) => {
    return (
        <>
            <LoginLogo />

            { children }
        </>
    )
}
