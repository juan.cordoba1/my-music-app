import React from 'react'

import logo from '../../../assets/spotifyIcon.svg';

export const LoginLogo = () => {
    return (
        <div className='auth__logoBox animate__animated animate__fadeInRight'>
            <img className='auth__logo' src={ logo } alt='Logo Spotify' />
            <p className='auth__title'>Your favorite music.</p>
            <p className='auth__title'>Free with Spotify.</p>
        </div>
    )
}
