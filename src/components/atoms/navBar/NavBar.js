import React from 'react'
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom'
import { authLogout } from '../../../redux/actions/auth';
import { musicLogout } from '../../../redux/actions/music';

export const NavBar = () => {

    const dispatch = useDispatch();

    const token = localStorage.getItem('token');

    const handleLogout = () => {
        dispatch(authLogout());
        dispatch(musicLogout());
        localStorage.removeItem('token');
    }

    return (
        <div className='nav__main'>
            <ul className='menu'>
                <NavLink
                    activeClassName="active"
                    className="menu__link" 
                    exact
                    to={ `/#access_token=${ token }` }
                >
                    <i className="fas fa-home"></i> Home
                </NavLink>
                <NavLink
                    activeClassName="active"
                    className="menu__link" 
                    exact
                    to="/favorites"
                >
                    <i className="fas fa-heart"></i> Favorites
                </NavLink>
                <hr/>
                <Link
                    className="menu__link" 
                    exact
                    to="/auth/login"
                    onClick={ handleLogout }
                >
                    <i className="fas fa-sign-out-alt"></i> Logout
                </Link>
            </ul>
        </div>
    )
}
