import React from 'react';

export const UserHeader = ({ name }) => {

    
    const handleClick = () => {
        const menu = document.querySelector('.nav__main');
        const menuIcon = document.querySelector('.fa-bars');
        menuIcon?.classList.toggle('dark');
        menu?.classList.toggle('show')
    }

    return (
        <div className='header__user'>
            <small>{ name }</small>
            <i className="fas fa-bars " onClick={ handleClick }></i>
        </div>
    )
}
