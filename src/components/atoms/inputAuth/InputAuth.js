import React from 'react'

export const InputAuth = ({ type, placeholder, name }) => {
    return (

        <input
            type={ type }
            placeholder={ placeholder }
            name={ name }
            autoComplete='off'
            className='auth__input'
        />

    )
}
