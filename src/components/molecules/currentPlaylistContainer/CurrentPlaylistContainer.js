import React from 'react'

export const CurrentPlaylistContainer = ({ playlist }) => {

    const { name, images /* description */ } = !!playlist && playlist[0];
    const image = !!images && images[0].url;

    return (
        <div className='currentPlaylist__container animate__animated animate__fadeInRight'>
            <img src={ `${ image }` } alt={ name } className='currentPlaylist__img'/>
            <h2 className='currentPlaylist__name'>{ name }</h2>
        </div>
    )
}
