import React from 'react';

export const TrackCard = ({ trackMain, handleFavorites }) => {

    let img = ''
    let artistsString = ''
    let nameTrack = ''
    let artistsNewList = [];
    let artistsMain = [];
    let idTrack = '';

    const { album, name, artists, id, isInFavorites } = trackMain;
    idTrack = id;
    artistsMain = [ ...artists ];
    nameTrack = name;

    const { images } = album;
    img = !!images && images[0].url

    const artistsList = artistsMain.map( (artist) => artist.name );

    artistsList.forEach( (artist, index) => {
        
        if( index === 3 ) {
            artistsNewList.push(' y mas...');
        }else if( index > 3 ) {
            return;
        }else {
            artistsNewList.push( ' ' + artist )
        }
    })
    artistsString = artistsNewList.toString();

    return (
        <div className='track__card'>
            <div className='track__img-title'>
                <img src={ `${ img }` } alt={ nameTrack } className='track__img'/>
                <div className='track__title'>
                    <h3>{ nameTrack }</h3>
                    <strong>Artists: <small>{ artistsString }</small></strong>
                </div>               
            </div>
            <i 
                className={ 
                    (isInFavorites)
                        ? 'fas fa-heart fa-sm'
                        : 'far fa-heart fa-sm'
                }
                onClick={ () => handleFavorites(idTrack) }
            >                
            </i>
        </div>
    )
}
