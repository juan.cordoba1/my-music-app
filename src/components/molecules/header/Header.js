import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

import logo from '../../../assets/spotifyIcon.svg'
import { UserHeader } from '../../atoms/userHeader/UserHeader'

export const Header = () => {

    const { user } = useSelector(state => state.auth)

    const display_name = !!user && user.user.display_name;

    const token = localStorage.getItem( 'token' );

    return (
        <header className='header__main'>
            <Link to={ `/#access_token=${ token }` }><img src={ logo } alt={ `${logo}` } className='header-logo'/></Link>

            <UserHeader name={ display_name }/>
    
        </header>
    )
}
