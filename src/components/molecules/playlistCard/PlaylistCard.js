import React from 'react'
import { Link } from 'react-router-dom';

export const PlaylistCard = ({ playlist }) => {

    const { id, name, images /* description */ } = !!playlist && playlist;
    const image = !!images && images[0].url;

    return (
        <Link className='playlist__card' to={ `/playlist/${ id }` } >
            <img src={ `${ image }` } alt={`${ name }`} className='playlist__card-img'/>
            <div className='playlist__card-name'>{name}</div>
        </Link>
    )
}
