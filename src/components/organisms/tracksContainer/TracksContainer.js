import React from 'react';
import { TrackCard } from '../../molecules/trackCard/TrackCard'
import { Loading2 } from '../../atoms/loading/loading2/Loading2';

export const TracksContainer = ({ track, handleFavorites, iconClass }) => {

    if( !track ) {
        return (
            <Loading2 />
        )
    }

    return (
        <div className='tracks__container animate__animated animate__fadeIn'>

            {
                track?.map( (track, index) => (
                    <TrackCard key={ index } trackMain={ track } handleFavorites={ handleFavorites } iconClass={ iconClass } />
                ))
            }
            
        </div>
    )

}
