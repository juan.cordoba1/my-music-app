import React from 'react'
import { Link } from 'react-router-dom'
import { InputAuth } from '../../atoms/inputAuth/InputAuth'

export const FormRegister = () => {

    const inputs = [{type:'text', placeholder:'Email', name:'email'}, {type:'text', placeholder:'UserName', name:'userName'}, {type:'password', placeholder:'Confirm password', name:'password2'}, {type:'password', placeholder:'Password', name:'password'}]

    return (
        <form className='auth__form animate__animated animate__fadeInRight'>
            
            {
                inputs.map( (input, index) => (
                    <InputAuth key={ index } type={ input.type } placeholder={ input.placeholder } name={ input.name } />
                ))
            }

            <button
                type='submit'
                className='auth__btn btn-login'
            >
                Register
            </button>

            <Link
                to='/auth/login'
                className='auth__link-register'
            >
                Already registered ?
            </Link>
        </form>
    )
}
