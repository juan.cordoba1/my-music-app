import React from 'react'
import { Link } from 'react-router-dom'
import { URL } from '../../../helpers/loginSpotifyConfig';
import { InputAuth } from '../../atoms/inputAuth/InputAuth';

export const FormLogin = () => {

    const inputs = [{type:'text', placeholder:'Email', name:'email'}, {type:'password', placeholder:'Password', name:'password'}]

    return (
        <form className='auth__form animate__animated animate__fadeInRight'>
            
            {
                inputs.map( (input, index) => (
                    <InputAuth key={ index } type={ input.type } placeholder={ input.placeholder } name={ input.name } />
                ))
            }

            <button
                type='submit'
                className='auth__btn btn-login'
            >
                Sign In
            </button>
            
            <a
                href={ URL }
                className='auth__btn btn-spotify'
            >
                Sign in with Spotify
            </a>

            <Link 
                to='/auth/register'
                className='auth__link-register'
            >
                Create new account
            </Link>
        </form>
    )
}
