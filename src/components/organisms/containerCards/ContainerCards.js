import React from 'react'
import { useSelector } from 'react-redux';
import { PlaylistCard } from '../../molecules/playlistCard/PlaylistCard';
import { Loading } from '../../atoms/loading/Loading';

export const ContainerCards = () => {

    const { playlists } = useSelector(state => state.music)

    if( !playlists ) {
        return (
            <Loading />
        )
    }

    return (
        <div className='container__cards animate__animated animate__fadeIn'>
    
           {
               playlists?.map( playlist => (
                   <PlaylistCard key={ !!playlist && playlist.id } playlist={ playlist } />
               ))
           }

        </div>
    )
}
