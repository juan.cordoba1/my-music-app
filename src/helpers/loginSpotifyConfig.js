
const endpoint = 'https://accounts.spotify.com/authorize';
const clientID = '055994d739c641928523202b8b7770d0';
const redirectUri = 'http://localhost:3000/';
const scopes = [
    'user-read-currently-playing',
    'user-read-recently-played',
    'user-read-playback-state',
    'user-read-playback-position',
    'user-read-email',
    'user-read-private',
    'user-top-read',
    'user-modify-playback-state',
    'user-library-read',
    'user-library-modify',
    'user-follow-read',
    'user-follow-modify',
    'playlist-modify-private',
    'playlist-modify-public',
    'playlist-read-private',
    'playlist-read-collaborative',
    'app-remote-control',
    'streaming',
    'ugc-image-upload'
];

export const URL = `${endpoint}?client_id=${clientID}&response_type=token&redirect_uri=${redirectUri}&scope=${scopes.join("%20")}&show_dialog=true`