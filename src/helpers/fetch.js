
const tokenStore = localStorage.getItem('token')
export const fetchApi = ( endpoint, method = 'GET', token = tokenStore ) => {

    return fetch( endpoint, {
        method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })

}