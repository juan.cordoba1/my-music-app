import React from 'react'
import { FormRegister } from '../components/organisms/formRegister/FormRegister'
import { Auth } from '../components/templates/auth/Auth'

export const RegisterScreen = () => {
    return (
        <Auth>

            <FormRegister />
        </Auth>
    )
}
