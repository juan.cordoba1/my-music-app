import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { DeleteFavoriteTrack, favoritesTracksDeleteStore, favoritesTracksSave, getFavoritesTracks } from '../redux/actions/music'
import { CurrentPlaylistContainer } from '../components/molecules/currentPlaylistContainer/CurrentPlaylistContainer'
import { TracksContainer } from '../components/organisms/tracksContainer/TracksContainer'
import { Playlist } from '../components/templates/playlist/Playlist'

export const Favorites = () => {

    const dispatch = useDispatch();
    const { favoritesTracks } = useSelector(state => state.music)

    const token = localStorage.getItem('token');
    useEffect(() => {
        
        dispatch(getFavoritesTracks(token));

        return () => {
            dispatch(favoritesTracksDeleteStore());
        }

    }, [])

    const currentPlaylist = [{
        name: 'My favorites tracks',
        images: [{
            url: 'https://t.scdn.co/images/3099b3803ad9496896c43f22fe9be8c4.png'
        }]
    }]

    const handleFavorites = (idTrack) => {

        const updateFavoritesTracks = favoritesTracks.filter( track => track.id !== idTrack)
        dispatch(favoritesTracksSave(updateFavoritesTracks));

        dispatch(DeleteFavoriteTrack(token, idTrack))

    }

    return (
        <Playlist>

            <CurrentPlaylistContainer playlist={ currentPlaylist }/>

            <TracksContainer track={ favoritesTracks } handleFavorites={ handleFavorites } iconClass='fas fa-heart' />
        </Playlist>
    )
}
