import React from 'react'
import { FormLogin } from '../components/organisms/formLogin/FormLogin'
import { Auth } from '../components/templates/auth/Auth'

export const LoginScreen = () => {
    return (
        <Auth>
            
            <FormLogin />           
        </Auth>
    )
}
