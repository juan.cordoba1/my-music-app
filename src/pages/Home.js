import React, { useEffect } from 'react'
import { useLocation } from "react-router";

import queryString from "query-string";
import { getUserProfile } from '../redux/actions/auth';
import { getPlaylist } from '../redux/actions/music';
import { useDispatch } from 'react-redux';

import { ContainerCards } from '../components/organisms/containerCards/ContainerCards';
import { Playlist } from '../components/templates/playlist/Playlist';

export const Home = () => {

    const dispatch = useDispatch()
    const location = useLocation();
    
    useEffect( () => {
        // Exatrer el access_token de la url y guardar en localStorage
        const { access_token = '' } = queryString.parse( location.hash );
        localStorage.setItem('token', access_token);
        
        // Obtener info de usuario de la api
        dispatch(getUserProfile(access_token));

        // Obtener playlists del usuario
        dispatch(getPlaylist(access_token))

    }, [dispatch, location.hash])

    return (
        <Playlist>

            <ContainerCards />            
        </Playlist>
    )
}
