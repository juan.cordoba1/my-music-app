import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router'
import { deleteCurrentPlaylist, getTracksPlaylist, setCurrentPlaylist, setFavoritesTracks, tracksDelete, tracksSave } from '../redux/actions/music';
import { CurrentPlaylistContainer } from '../components/molecules/currentPlaylistContainer/CurrentPlaylistContainer';
import { TracksContainer } from '../components/organisms/tracksContainer/TracksContainer';
import { Playlist } from '../components/templates/playlist/Playlist'

export const PlaylistTracks = () => {

    const { playlistId } = useParams();
    const dispatch = useDispatch();
    const { tracks } = useSelector(state => state.music)

    const { playlists, currentPlaylist } = useSelector(state => state.music)
    const currentPlaylistFiltered = playlists?.filter( item => item.id === playlistId);  
    
    const token = localStorage.getItem('token');
    useEffect(() => {
        
        dispatch(getTracksPlaylist(token, playlistId));

        dispatch(setCurrentPlaylist(currentPlaylistFiltered));

        return () => {
            dispatch(tracksDelete());
            dispatch(deleteCurrentPlaylist());
        }

    }, [dispatch, playlistId])

    const handleFavorites = (idTrack) => {

        const updateTracks = tracks.map( track => {
            if( track.id === idTrack ) {
                return ({
                    ...track,
                    isInFavorites: true
                })
            }else {
                return ({
                    ...track
                })
            }
        })

        dispatch(tracksSave(updateTracks));

        dispatch(setFavoritesTracks(token, idTrack));
        /* dispatch(getTracksPlaylist(token, playlistId)); */

    }

    return (
        <Playlist>
            <CurrentPlaylistContainer playlist={ currentPlaylist }/>

            <TracksContainer track={ tracks } handleFavorites={ handleFavorites } iconClass='far fa-heart' />
        </Playlist>
    )
}
