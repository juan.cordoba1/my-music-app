import React from 'react';
import ReactDOM from 'react-dom';
import { MyMusicApp } from './MyMusicApp';


import './styles/styles.scss';
import './App.css'

ReactDOM.render(
    <MyMusicApp />,
  document.getElementById('root')
);

